import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { IpcService } from '../services/ipc.service';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent implements OnInit {
  title = 'DemoElectron';
  response: boolean = false;
  message: string = '';
  count: number = 0;

  constructor(
    private ipcService: IpcService,
    private cdRef: ChangeDetectorRef
  ) {}
  ngOnInit(): void {}

  send(): void {
    this.ipcService.send('message', 'Enviando mensaje');
    this.ipcService.on('reply', (event: any, arg: string) => {
      this.response = arg === 'reply_message';
      this.cdRef.detectChanges();
      this.count = this.count + 1;

      this.message = 'Se ha recibido una respuesta al envio del mensaje.'
    });
  }

  ngOnDestroy(): void {
    this.ipcService.removeAllListeners('reply');
  }
}
